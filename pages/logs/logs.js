
var app = getApp();
Page({
  data: {
    logs:[],
    searchIput:'',
    logInnerIput:'',
    switch1Input:false,
    showAddLogModal: false,
    showBgModal:false,
  },
  onLoad: function () {
    wx.showLoading({
      title: '加载中',
    })
    var that = this;
    wx.request({
      url: 'https://www.jimuworld.cn/api/card/logslist', 
      data: {
        x: '',
        y: ''
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success(res) {
        var data = res.data.data;
        that.setData({
          logs: data
        })
        console.log(data)
        wx.hideLoading()
        wx.showToast({
          title: '成功',
          icon: 'success',
          duration: 500
        })
      }
    })
  },

  // 打开添加记录弹窗
  openAddLog: function () {
    this.setData({
      showAddLogModal: true,
      showBgModal: true
    })
  },
  // 关闭弹窗
  close: function () {
    this.setData({
      showAddLogModal: false,
      showBgModal: false
    })
  },
  // 编辑记录
  del: function (e) {
    var that = this;
    wx.showActionSheet({
      itemList: ['删除', '添加提醒', '取消提醒'],
      success(res) {
        // 如果是删除，则确认
        if (res.tapIndex == 0){
          wx.showModal({
            title: '提示',
            content: '确定删除这条记录吗？',
            success(res) {
              if (res.confirm) {

                // 删除操作开始
                wx.showLoading({  title: '删除中' })
               
                wx.request({
                  url: 'https://www.jimuworld.cn/api/card/dellog',
                  data: { id: e.currentTarget.id },
                  header: { 'content-type': 'application/json' },
                  method:'post',
                  success(res) {
                    var data = res.data
                    wx.hideLoading()
                    wx.showToast({
                      title: data.msg,
                      icon: 'none',
                      duration: 500
                    })
                    that.onLoad();
                  }
                })
                // 删除结束
              } else if (res.cancel) {
                return;
              }
            }
          })
        }else{

          // 修改状态
          wx.showLoading({ title: '请稍后' })
          var c = this;
          wx.request({
            url: 'https://www.jimuworld.cn/api/card/updatelogstatus',
            data: { id: e.currentTarget.id, warn: res.tapIndex},
            header: { 'content-type': 'application/json' },
            method: 'post',
            success(res) {
              var data = res.data
              if (data.code == 200) {
                wx.hideLoading()
              }
              wx.showToast({
                title: data.msg,
                icon: 'none',
                duration: 500
              })
              that.onLoad();
            }
          })

        }
        
        // console.log(e.currentTarget.id)
        // console.log(res.tapIndex)
      }
    })
  }, 
  // 提交新的记录
  submit: function() {
    app.submit(this.data.logInnerIput,this.data.switch1Input);
    this.onLoad();
    this.close();
  },
  // 获取输入的内容
  logInnerIput: function (e) {
    this.setData({
      logInnerIput: e.detail.value
    })
  }, 
  // 获取搜索内容
  searchIput: function (e) {
    this.setData({
      searchIput: e.detail.value
    })
  }, 
  seachSubmit: function () {
    var keyword = this.data.searchIput;
    if (!keyword) {
      wx.showToast({
        title: '请输入内容！',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    var that = this;
    wx.request({
      url: 'https://www.jimuworld.cn/api/card/searchLogs',
      data: { keyword: keyword },
      method: 'post',
      header: { 'content-type': 'application/json' },
      success(res) {
        console.log(res)
        wx.hideLoading()
        wx.showToast({
          title: res.data.msg,
          icon: 'none',
          duration: 2000
        })
        if (res.data.code == 200) {
          var data = res.data.data;
          that.setData({
            logs: data
          })
          return;
        }
      }
    })

  },
  // 是否提醒
  switch1Change: function (e) {
    this.setData({
      switch1Input: e.detail.value
    })
  },
})
