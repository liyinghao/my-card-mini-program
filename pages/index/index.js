var app = getApp();
Page({
  data: {
    day: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
    // 打卡数据
    card_status: 0,
    card_info: '',
    card_time: '',
    card_userid: 0,
    card_id: 0,
    // 添加用户数据
    addUserName: '',
    showAddLogModal: false,
    showLogModal: false,
    showBgModal: false,
    showAddUserModal: false,
    writeTime: '',
    importantNotes:'没有重要的提醒···',
    array: [{ msg: '1' }, { msg: '2' }],
    multiArray: [['2018'], ['1']],
    multiIndex: [0, 0],
    user:[1]
  },
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
    })
    var that = this;
    wx.request({
      url: 'https://www.jimuworld.cn/api/card/cardindex',
      data: {  x: '' },
      method: 'post',
      header: {  'content-type': 'application/json'  },
      success(res) {
        var data = res.data.data;
        that.setData({
          writeTime: data.cardAlert.time,
          importantNotes: data.cardAlert.record,
          multiArray: data.nowTime,
          multiIndex: data.multiIndex,
          user: data.user
        })
        // console.log(data.user[0])
        wx.hideLoading()
        wx.showToast({
          title: '成功',
          icon: 'success',
          duration: 500
        })
      }
    })
  },
  // 打卡提交操作
  cardoperate: function (e) {
    var userid = this.data.card_userid;
    if (!userid || !this.data.card_time){
      wx.showToast({
        title: '信息异常，请重新打开',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    var that = this;
    wx.request({
      url: 'https://www.jimuworld.cn/api/card/cardoperate',
      data: { userid: userid, time: this.data.card_time, status: this.data.card_status, id: this.data.card_id},
      method: 'post',
      header: { 'content-type': 'application/json' },
      success(res) {
        console.log(res)
        wx.hideLoading()
        wx.showToast({
          title: res.data.msg,
          icon: 'success',
          duration: 1000
        })
        if (res.data.code == 200){
          that.close();
          that.onLoad();
          return;
        }
      }
    })
  
  },
  // 打卡类型切换
  cardType: function (e) {
    this.setData({
      card_status: e.currentTarget.id
    })
  },
  // 打卡文字描述获取文字
  cardInfoIput: function (e) {
    this.setData({
      card_info: e.detail.value
    })
  },
  // 打开记录列表页面
  openLogsPage: function (e) {
    wx.switchTab({    //保留当前页面，跳转到应用内的某个页面（最多打开5个页面，之后按钮就没有响应的）
      url: "/pages/logs/logs"
    })
  },
  // 打卡弹窗
  openCard: function (e) {
    var dat = e.currentTarget.dataset.index;
    console.log(e.currentTarget)
    this.setData({
      showLogModal: true,
      showBgModal: true,
      card_status: dat.status ? dat.status: 0,
      card_info: dat.info ? dat.info: '',
      card_time: dat.nowtime,
      card_userid: e.currentTarget.id,
      card_id: dat.id ? dat.id: 0
    })
  },
  // 添加记录弹窗
  openAddLog: function () {
    this.setData({
      showAddLogModal: true,
      showBgModal: true
    })
  },

  // 添加用户弹窗
  openAddUser: function () {
    this.setData({
      showAddUserModal: true,
      showBgModal:true
    })
  },
  // 添加用户 获取输入的用户名
  addUserIput: function (e) {
    this.setData({
      addUserName: e.detail.value
    })
  },
  // 添加用户提交操作
  adduseroperate: function (e) {
    var name = this.data.addUserName;
    if (!name) {
      wx.showToast({
        title: '名字不可以是空~',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    wx.showLoading({  title: '加载中' })
    var that = this;
    wx.request({
      url: 'https://www.jimuworld.cn/api/card/adduser',
      data: { name: name, pid: 1},
      method: 'post',
      header: { 'content-type': 'application/json' },
      success(res) {
        console.log(res)
        wx.hideLoading()
        wx.showToast({
          title: res.data.msg,
          icon: 'success',
          duration: 1000
        })
        if (res.data.code == 200) {
          that.close();
          that.onLoad();
          return;
        }
      }
    })

  },

  // 关闭弹窗
  close: function () {
    this.setData({
      showAddLogModal: false,
      showBgModal: false,
      showAddUserModal: false, showLogModal:false
    })
  },
  // 提交新的记录
  submit: function () {
    app.submit(this.data.logInnerIput, this.data.switch1Input);
    this.onLoad();
    this.close();
  },
  bindMultiPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      multiIndex: e.detail.value
    })
  },
  bindMultiPickerColumnChange: function (e) {
    console.log('修改的列为', e.detail.column, '，值为', e.detail.value);
    var data = {
      multiArray: this.data.multiArray,
      multiIndex: this.data.multiIndex
    };
    data.multiIndex[e.detail.column] = e.detail.value;
   
    this.setData(data);
  },
  // 获取输入的内容
  logInnerIput: function (e) {
    this.setData({
      logInnerIput: e.detail.value
    })
  },
  // 是否提醒
  switch1Change: function (e) {
    this.setData({
      switch1Input: e.detail.value
    })
  },
})